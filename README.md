http://gold.xitu.io/entry/574fe7c579bc440052f6d805
http://www.jianshu.com/p/42e11515c10f

(一) 基本使用
-- npm install webpack -g
-- npm install webpack --save-dev

-- webpack --watch 实时更新但是要手动刷新 


(二) 服务
-- webpack 提供了 webpack-dev-server 解决实时刷新页面的问题，同时解决实时构建的问题。
-- npm install webpack-dev-server -g

   执行 webpack-dev-server --inline   输入 http://localhost:8080

     或 webpack-dev-server  输入 http://localhost:8080/webpack-dev-server/


(三) 第三方库

-- npm install jquery --save //如在依赖中添加jQuery 引入就能用了

(四) 模块化

     -- 感叹号的作用在于使同一文件能够使用不同类型的loader

     (1) ec6 ( 利用 babel-loader 加载 es6 模块 )

      -- npm install babel-loader babel-core babel-preset-es2015 --save-dev

     (2) css ( style-loader(以style形式head) 及 css-loader（解读、加载css）)

      -- npm install style-loader css-loader --save-dev

     (3) 


