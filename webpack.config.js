var path = require("path")
var webpack = require("webpack")
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var autoprefixer = require('autoprefixer')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var OpenBrowserPlugin = require('open-browser-webpack-plugin')

var Dashboard = require('webpack-dashboard');
var DashboardPlugin = require('webpack-dashboard/plugin');
var dashboard = new Dashboard();

module.exports = {
	devtool: 'eval-source-map',//配置生成Source Maps，选择合适的选项 google可查看打包前的代码
    entry:{
	    index: './main.js',
	    index2: './main2.js'
	}, //入口配置
    output: {           //出口配置
        path: path.join(__dirname, "build"), // 输出文件的保存路径
        filename: '[name].bundle.js' // 输出文件的名称
    },
// hash：根据编译时资源对应的编译进程计算hash值 [hash:8] 生成八位随机数
// chunkhash：根据模块内容计算hash值  [chunkhash:8]
// contenthash：根据文件的内容计算hash值  [contenthash:8]

    module: {
        loaders: [
	        { 
		        test: /\.js$/,
		        loaders: ['babel?presets[]=es2015'],
		        exclude: /node_modules/,  //排除
		        include:"",//必须处理的
		        query:"" //提供额外的设置
	        },
	        {
				test: /\.css$/,
				loader: 'style!css?modules!autoprefixer?{browsers:["last 2 version", "> 1%"]}', //添加前缀 autoprefixer-loader 写法
				//...
			},
			{
		        test: /\.json$/,
		        loader: "json"
		    },
		    {
		        test: /\.(ico|jpg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
                loader: 'file'
		    },
		     {
                test: /\.scss$/,
                // include: path.join(__dirname, "sass"),
                exclude: /node_modules/,
                loader: 'style-loader!css-loader!postcss-loader!sass-loader?outputStyle=expanded'

            }
        ]
    },
    
    postcss: function() {
        return {
            defaults: [autoprefixer({
                browsers: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9', // React doesn't support IE8 anyway
                ]
            })]
        };
    },
    
    // externals: { //
    //     jquery: 'window.$'
    // },
   
    resolve:{
        extensions:['','.js',".css",'jsx'],  //自动补全识别后缀

        alias: {//快速指向指定的文件夹不要单选路径问题
                //需要注意的是：在scss之类的css预编译中引用要加上~，以便于让loader识别是别名引用路径。
            outPluginsJs: path.join(__dirname, "build/outPluginsJs") // outPluginsJs === build/outPluginsJs
        }
    },
    plugins: [  //插件
        new ExtractTextPlugin("[name].css"), //提取出来的样式放在style.css文件中
        new HtmlWebpackPlugin({
            //title:"首页",
            //filename:"",//默认index.html
            //inject: 'head', //指定引入生成的js的位置
            //favicon:'./images/favico.ico',
            //minify:true,
            //hash:true,
            //cache:false,
            //showErrors:false,
            //"head": {
            //  "entry": "assets/head_bundle.js",
            //  "css": [ "main.css" ]
            //},
            //xhtml:false
            template: path.join(__dirname, 'index.html'),
            chunks:['index','index2']
        }),
        new DashboardPlugin(dashboard.setData),
        new OpenBrowserPlugin({
            url: 'http://localhost:808'
        }),
        new webpack.ProvidePlugin({
            jQuery: "jquery",
            $: "jquery"
        })
    ],


    devServer: {
	    contentBase: "./build",//本地服务器所加载的页面所在的目录(默认当前)
	    colors: true,//终端中输出结果为彩色
	    historyApiFallback: true,//不跳转
	    inline: true,//实时刷新 可在命令行后跟随
	    port:"808"
	} 
};