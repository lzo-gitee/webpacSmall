// import './css/index.css';
import './sass/index.scss';
console.log(jQuery)
// 引入 ECharts 主模块
var echarts = require('echarts');
// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('Echart'));
// 绘制图表
myChart.setOption({
    title: { text: 'ECharts 入门示例' },
    tooltip: {},
    xAxis: {
        data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
    },
    yAxis: {},
    series: [{
        name: '销量',
        type: 'line',
        data: [5, 20, 36, 10, 10, 20]
    }]
});